# UCL -- Institute of Finance & Technology 2021/22
# **Big Data in Finance Git Repository**

---
**Navigate to the subfolder to see the code.**

*Clone this Git Repository to have your script always updated on your local machine*

## Contributing
---
This project welcomes contributions and suggestions. It is highly recommended to read and understand Coding Best Practices listed in the wiki attached to this repository [Link](https://bitbucket.org/uceslc0/iftcode2021/wiki/BestPractices.md)

Please, use following workflow:

+ Clone this repo ("git clone https://uceslc0@bitbucket.org/uceslc0/iftcode2020.git")

+ cd into the repo directory by:
	+ cd ./iftcode2020

+ create a new branch

	+ git checkout -b newBranchName

+ copy in your local repo your developments

+ Add, Committ and Push
	+ git add --all
	+ git commit -m "Message template to be Edited"
	+ git push -u origin newBranchName

+ Create a pull request